import os, sys
import site

site.addsitedir('/var/www/sites/{{ project_name }}/env/lib/python2.7/site-packages')
site.addsitedir('/var/www/sites/{{ project_name }}/webapp/lib')
site.addsitedir('/var/www/sites/{{ project_name }}/webapp')

sys.stdout = sys.stderr

os.environ['DJANGO_SETTINGS_MODULE'] = '{{ project_name }}.conf.dev.settings'
os.environ['DJANGO_WEB_ROOT']        = '/var/www/sites/{{ project_name }}/webroot'

import django.core.handlers.wsgi

application = django.core.handlers.wsgi.WSGIHandler()
