# ==================== Project Settings ====================
"""
Project settings that will be used across all of the environments go here.
Any settings that should be changed from the values in
{{ project_name }}.conf.defaults.settings should be overridden here.
"""

# import default settings
from .defaults.settings import *

ADMINS = (
#    ('Site Admin', 'site-admin@example.com'),
)
MANAGERS = ADMINS

# generate a secret key for each site
SECRET_KEY = '{{ secret_key }}'

ROOT_URLCONF = '{{ project_name }}.conf.urls'

INSTALLED_APPS = (
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.sites',
    'django.contrib.messages',
    'django.contrib.admin',
    'django.contrib.staticfiles',
    'south',
    '{{ project_name }}.apps.foundation',
)