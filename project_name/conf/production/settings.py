
# ==================== Production Settings ====================

# import project settings
from ..settings import *

DEBUG = False
TEMPLATE_DEBUG = DEBUG

# Set the allowed hosts to the host name(s) where this site will be accessible (e.g. 'www.smashingideas.com')
ALLOWED_HOSTS = ['{{ host_name }}']

ROOT_URLCONF = '{{ project_name }}.conf.production.urls'
