import os

app_name = os.environ['APP_NAME']
bind_address = os.environ['ADDRESS']
bind_port = os.environ['PORT']
workers = os.environ['GUNICORN_WORKERS']

bind = "%s:%s" % (bind_address, bind_port)
worker_class = "sync"
daemon = False
max_requests = 2000
