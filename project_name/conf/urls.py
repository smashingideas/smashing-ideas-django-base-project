# ==================== Project URLS ====================
"""
Place all project-wide URLs here. This file is included in the
environment-specific urls files (local, dev, etc.). You do not
need to include the admin URLs, as they are included as needed
by the environment-specific files.
"""

from django.conf.urls import patterns, include, url
from django.contrib.staticfiles.urls import staticfiles_urlpatterns

urlpatterns = patterns('',
) + staticfiles_urlpatterns()

