from django.conf.urls import patterns, include
from django.conf import settings

urlpatterns = patterns('',
   (r'', include('{{ project_name }}.conf.defaults.urls')),
   (r'', include('{{ project_name }}.conf.urls')),
)
