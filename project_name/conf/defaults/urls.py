from django.conf import settings
from django.conf.urls import patterns, include
from django.contrib import admin
from django.views.generic import RedirectView

admin.autodiscover()

urlpatterns = patterns('',
    (r'^admin/', include(admin.site.urls)),
    (r'^robots\.txt$', RedirectView.as_view(url=settings.STATIC_URL + 'robots.txt')),
    (r'^favicon\.ico$', RedirectView.as_view(url=settings.STATIC_URL + 'favicon.ico')),
)