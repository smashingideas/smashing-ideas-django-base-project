import os, sys

PROJECT_DIR = os.path.realpath(os.path.dirname(os.path.dirname(__file__)))
LIBRARY_DIR = os.path.join(PROJECT_DIR, 'lib')

sys.path.append(LIBRARY_DIR)
sys.path.append(PROJECT_DIR)

os.environ.setdefault('DJANGO_SETTINGS_MODULE', '{{ project_name }}.conf.local.settings')

from django.core.wsgi import get_wsgi_application

application = get_wsgi_application()
